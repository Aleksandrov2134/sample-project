## Description
Hizzle test task

### ui-screenshots are located in "screenshots" directory

## Stack
##### Backend: NestJS
##### DB: PostgreSQL
##### ORM: TypeORM
##### Frontend: VueJS

##### ormconfig-path: backend/ormconfig.json

#### Note: Before running commands need to have postgresql server installed and set up by ormconfig.json

# Backend

### Installation

```bash
# in "backend" directory

$ npm install
```

### Running the app

```bash
# in "backend" directory

$ npm run start
```

### Running migrations

```bash
# in case your postgresql db was set up
# run command below after nestjs server is started
# this command will init sample data in DB

$ typeorm migration:run
```

# Frontend

### Installation

```bash
# in "frontend" directory

$ npm install
```

### Running the app

```bash
# in "frontend" directory

$ npm run serve
```

## License

  Nest is [MIT licensed](LICENSE).
