import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Property } from './property.entity';

/**
 * entity represents "offer" table in DB
 * includes Many-To-One relation with table "property"
 * uses Fetch Type - EAGER to always fetch property record for each offer
 */
@Entity()
export class Offer {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  offered_by: string;

  @Column()
  price: number;

  @ManyToOne(type => Property, property => property.offers, {eager: true})
  property: Property;
}
