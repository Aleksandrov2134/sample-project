import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { PropertyKindEnum } from '../enum/property-kind.enum';
import { PropertyLocationEnum } from '../enum/property-location.enum';
import { OneToMany } from 'typeorm';
import { Offer } from './offer.entity';

/**
 * entity represents "property" table in DB
 * includes One-To-Many relation with table "offer"
 * uses Fetch Type - Lazy (by default)
 * thus, to fetch all related offers we have to do that manually
 */
@Entity()
export class Property {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  area: number;

  @Column()
  bedrooms: number;

  @Column({
    type: 'enum',
    enum: PropertyKindEnum,
  })
  kind: PropertyKindEnum;

  @Column({
    type: 'enum',
    enum: PropertyLocationEnum,
  })
  location: PropertyLocationEnum;

  @OneToMany(type => Offer, offer => offer.property)
  offers: Offer[];
}
