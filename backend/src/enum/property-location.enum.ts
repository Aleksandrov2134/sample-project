/**
 * enum represents all possible variations
 * for property' location
 */
export enum PropertyLocationEnum {
  'NEW_YORK'    = 'New York',
  'LONDON'      = 'London',
  'PARIS'       = 'Paris',
  'TOKYO'       = 'Tokyo',
  'ATHENS'      = 'Athens',
  'BERLIN'      = 'Berlin',
  'LIMASSOL'    = 'Limassol',
  'DUBAI'       = 'Dubai',
  'ROME'        = 'Rome',
  'LOS_ANGELES' = 'Los Angeles',
  'BARCELONA'   = 'Barcelona',
  'MOSCOW'      = 'Moscow'
}
