/**
 * enum represents all possible variations
 * for property' kind
 */
export enum PropertyKindEnum {
  HOUSE     = 'House',
  APARTMENT = 'Apartment',
  STUDIO    = 'Studio',
  VILLA     = 'Villa'
}