import { Injectable } from '@nestjs/common';
import { OfferRepository } from '../repository/offer.repository';
import { Offer } from '../entity/offer.entity';

/**
 * service represents methods related to Offers
 */
@Injectable()
export class OfferService {

  constructor(private offerRepository: OfferRepository) {}

  /**
   * fetches all rows from table "offer"
   * using {@link OfferRepository}
   */
  getAll(): Promise<Offer[]> {
    return this.offerRepository.find();
  }
}
