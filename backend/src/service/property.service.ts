import { Injectable } from '@nestjs/common';
import { PropertyRepository } from '../repository/property.repository';
import { Property } from '../entity/property.entity';

/**
 * service represents methods related to Properties
 */
@Injectable()
export class PropertyService {

  constructor(private propertyRepository: PropertyRepository) {}

  /**
   * fetches all rows from table "property"
   * using {@link PropertyRepository}
   */
  getAll(): Promise<Property[]> {
    return this.propertyRepository.find();
  }
}
