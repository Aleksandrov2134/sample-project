import { EntityRepository, Repository } from 'typeorm';
import { Property } from '../entity/property.entity';

/**
 * repository class relates to "property" table
 */
@EntityRepository(Property)
export class PropertyRepository extends Repository<Property> {
}
