import { EntityRepository, Repository } from 'typeorm';
import { Offer } from '../entity/offer.entity';

/**
 * repository class relates to "offer" table
 */
@EntityRepository(Offer)
export class OfferRepository extends Repository<Offer> {
}
