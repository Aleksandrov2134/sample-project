import { Controller, Get } from '@nestjs/common';
import { PropertyService } from '../service/property.service';
import { Property } from '../entity/property.entity';

/**
 * controller represents endpoints related to Properties
 * includes prefix "/properties"
 */
@Controller('properties')
export class PropertyController {
  constructor(private readonly propertyService: PropertyService) {}

  /**
   * GET "/api/properties" will return all rows from table "property"
   */
  @Get()
  getAll(): Promise<Property[]> {
    return this.propertyService.getAll();
  }
}
