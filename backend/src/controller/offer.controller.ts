import { Controller, Get } from '@nestjs/common';
import { OfferService } from '../service/offer.service';
import { Offer } from '../entity/offer.entity';

/**
 * controller represents endpoints related to Offers
 * includes prefix "/offers"
 */
@Controller('offers')
export class OfferController {
  constructor(private readonly offerService: OfferService) {}

  /**
   * GET "/api/offers" will return all rows from table "offer"
   */
  @Get()
  getAll(): Promise<Offer[]> {
    return this.offerService.getAll();
  }
}
