import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // set '/api' prefix for every endpoint
  app.setGlobalPrefix('/api');
  // use 8080 port for backend
  await app.listen(8080);
}
bootstrap();
