import { Module } from '@nestjs/common';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { PropertyModule } from './module/property.module';
import { OfferModule } from './module/offer.module';
import * as config from '../ormconfig.json';

/**
 * main module which is used to bootstrap the app
 */
@Module({
  imports    : [
    TypeOrmModule.forRoot(config as TypeOrmModuleOptions),
    PropertyModule,
    OfferModule,
  ]
})
export class AppModule {
}
