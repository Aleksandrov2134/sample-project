import { PropertyRepository } from '../repository/property.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PropertyController } from '../controller/property.controller';
import { PropertyService } from '../service/property.service';
import { Module } from '@nestjs/common';

/**
 * module which represents all layers related to Properties
 */
@Module({
  imports    : [TypeOrmModule.forFeature([PropertyRepository])],
  controllers: [PropertyController],
  providers  : [PropertyService],
})
export class PropertyModule {
}