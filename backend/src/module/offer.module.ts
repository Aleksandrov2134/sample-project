import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { OfferRepository } from '../repository/offer.repository';
import { OfferController } from '../controller/offer.controller';
import { OfferService } from '../service/offer.service';


/**
 * module which represents all layers related to Offers
 */
@Module({
  imports    : [TypeOrmModule.forFeature([OfferRepository])],
  controllers: [OfferController],
  providers  : [OfferService]
})
export class OfferModule {
}