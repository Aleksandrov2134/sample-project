## Description
Hizzle test task

<b>ui-screenshots are located in "screenshots" directory</b>

## Stack
Backend: NestJS <br>
DB: PostgreSQL <br>
ORM: TypeORM <br>
Frontend: VueJS <br>

ormconfig-path: backend/ormconfig.json

#### Note: Before running commands need to have postgresql server installed and set up by ormconfig.json

# Backend

### Installation

```bash
# in "backend" directory

$ npm install
```

### Running the app

```bash
# in "backend" directory

$ npm run start
```

### Running migrations

```bash
# in case your postgresql db was set up
# run command below after nestjs server is started
# this command will init sample data in DB

$ typeorm migration:run
```

## License

  Nest is [MIT licensed](LICENSE).
