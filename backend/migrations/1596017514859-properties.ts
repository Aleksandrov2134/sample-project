import { MigrationInterface, QueryRunner } from 'typeorm';

/**
 * migration to fill "property" table with sample data
 */
export class properties1596017514859 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'London', 2, 120);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Paris', 2, 100);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Tokyo', 3, 90);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('House', 'Athens', 4, 150);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Studio', 'Berlin', 1, 70);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Villa', 'Limassol', 5, 300);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Dubai', 2, 120);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Rome', 2, 90);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Los Angeles', 2, 105);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('House', 'Barcelona', 3, 210);
      INSERT INTO property (kind, location, bedrooms, area)
      VALUES ('Apartment', 'Moscow', 3, 140);
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
