import { MigrationInterface, QueryRunner } from 'typeorm';

/**
 * migration to fill "offer" table with sample data
 */
export class offers1596018129230 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Neo', 400000, 1);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Morpheus', 300000, 3);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Agent Smith', 1200000, 7);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Trinity', 250000, 9);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('The Oracle', 430000, 10);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Cypher', 340000, 11);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Apoc', 350000, 2);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Agent Jones', 280000, 3);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Tank', 190000, 6);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Dozer', 500000, 4);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Agent Brown', 350000, 11);
      INSERT INTO offer (offered_by, price, "propertyId")
      VALUES ('Switch', 320000, 3);
    `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
