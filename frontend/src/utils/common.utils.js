/**
 * mapping response to pass json data
 * @param response
 * @returns {*}
 */
export const mapResponseToData = response => response.data;
