import { HTTP } from '@/main';
import { mapResponseToData } from '@/utils/common.utils';

/**
 * service represents methods related to property
 */
export const PropertyService = {
    /**
     * get all "property" records
     */
    getAll() {
        return HTTP.get('/properties').then(mapResponseToData);
    }
};
