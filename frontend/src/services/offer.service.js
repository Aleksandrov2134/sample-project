import { HTTP } from '@/main';
import { mapResponseToData } from '@/utils/common.utils';

/**
 * service represents methods related to offers
 */
export const OfferService = {
    /**
     * get all "offer" records
     */
    getAll() {
        return HTTP.get('/offers').then(mapResponseToData);
    }
};
