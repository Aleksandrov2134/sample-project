import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';

/**
 * axios instance to use same instance allover the project
 */
export const HTTP = axios.create({
    // adding "/api" prefix for each request
    baseURL: '/api'
});

/**
 * interceptor to catch response errors
 * and emit them inside the app
 * so we can show respective notifications
 */
HTTP.interceptors.response.use(
    response => response,
    error => {
        HizzleApp.$root.$emit('error', error.response.data.message);
        return Promise.reject(error);
    }
);
Vue.config.productionTip = false;

/**
 * app instance
 */
const HizzleApp = new Vue({
    render: h => h(App)
}).$mount('#app');
