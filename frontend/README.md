## Description
Hizzle test task

<b>ui-screenshots are located in "screenshots" directory</b>

## Stack
Backend: NestJS <br>
DB: PostgreSQL <br>
ORM: TypeORM <br>
Frontend: VueJS <br>

ormconfig-path: backend/ormconfig.json

#### Note: Before running commands need to have postgresql server installed and set up by ormconfig.json

### Installation

```bash
# in "frontend" directory

$ npm install
```

### Running the app

```bash
# in "frontend" directory

$ npm run serve
```
